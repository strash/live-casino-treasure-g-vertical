extends Control


signal btn_info_back_pressed


enum { HIDE, MENU, CONTENT }
var view: int = HIDE
var input_relative: float = 0.0
var grid_pos: float
var tween_speed: float = 0.15


var TITLES: PoolStringArray = [
	"Provider",
	"Evolution Gaming",
	"NETENT",
	"BETGAMES",
	"Lucky Streak",
	"Ezugi",
]

var TEXTS: PoolStringArray = [
	"",
	"The stable EGR Live Casino Supplier of the Year, Evolution Gaming can be rightfully called the leader – at least because of the fact that this provider was honoured the first rank in this category from 2010 to 2017. The story of Evolution’s success started earlier, though. The company was founded in 2006 in Sweden, at first collaborating with other operators. However, in three years’ time, their own first studio in Riga, Latvia was opened. Now, its market capitalization equals EUR 1,6 bln. Evolution Gaming offers a wide variety of classical and innovative games (including different versions of Blackjack, Roulette and Baccarat), due to which various player groups are matched. The company provides the widest selection of tables (300 tables by the end of 2016) – including the option of branded tables, and impressive dealer range (around 3000), including native-speaking English, French, Swedish, Turkish, Russian, Danish, Greek, and Italian dealers. Live sessions are broadcasted from ultra-modern studios in an excellent video quality.",
	"NetEnt is a famous provider which both produces high-end slot games and streams world-class live games in HD. Founded in 1996 in Stockholm, now the company’s market capitalization is over EUR 1 bln, as provided in the Netent’s annual report of 2016. The company’s revenue has increased by 28.5% from 2015 to 2016, and continues to do so each year. In fact, it is considered to be the best slot provider, supplying the market with new games on a monthly basis, licensing such famous titles as Aliens, Scarface and South Park and having obtained prestigious prizes, including awards on EGR 2016. As to NetEnt’s Live Casino solution, it is developed in collaboration with real players to ensure high-end player experience. The Live casino solution features the following key points: aimed at 22 nationalities; features flawless graphics and sounds; Chroma key technology ensures smooth integration of products into operators’ casino environments; supports both landscape and portrait modes; provides player acquisition and retention tools.",
	"BetGames main distinction from other igaming providers is its game range. This Lithuania based provider offers such non-standard games with variable betting rules as Dice, Lucky 5, Bet on Poker, Bet on Baccarat, Wheel of Fortune, War of Bets, Lucky 7, Lucky 6 and Dice Duel. The company was established in 2012, and have grown into an enterprise with over 100 professionals in 5 years. BetGames is now offering their services to 30 countries over the world, mainly in Eastern Europe and Africa. Partly, their success is based on the fact that BetGames pay specific attention to their dealers, who are top-rank professionals, experienced at every level. Choosing the content this provider produces, you can expect live broadcasts each 3 – 5 minutes, a “multi-table effect” and personal configurations.",
	"Lucky Streak is one more upscale live casino dealer, founded in 2014. Their studio is located in Riga, Latvia, from where European Roulette, Baccarat and Blackjack are streamed. The quality of streaming is ensured by using the newest web technologies and premium video streaming components. Apart from this, Lucky Streak offers slots, fixed odds games and card games. Always striving for the best and being managed by experts of online behaviour, Lucky Streak provides gorgeous interface, gamification features and different bet options.",
	"Ezugi is a leading provider that offers live casino games streamed in high quality from luxurious studios. Oriented at European, South American and Asian markets, at present, this provider offers service in English and Spanish languages. Its pride is its interactive features. For example, players can communicate with each other during the play, post Facebook posts or play several games simultaneously. The live casino games offered by Ezugi’s are as follows: Blackjack, Baccarat, Roulette, Lottery, RNG games and mini-games.",
]


# BUILTINS - - - - - - - - -


func _ready() -> void:
	grid_pos = ($Grid as VBoxContainer).rect_position.y
	set_view()


func _input(event: InputEvent) -> void:
	if view == MENU:
		if event is InputEventScreenDrag:
			($Grid as VBoxContainer).rect_position.y += event.relative.y
			input_relative = event.relative.y
		elif event is InputEventScreenTouch and not event.is_pressed():
			var _t: int
			if ($Grid as VBoxContainer).rect_position.y > grid_pos:
				_t = ($Tween as Tween).interpolate_property($Grid as VBoxContainer, "rect_position:y", null, grid_pos, tween_speed)
			elif ($Grid as VBoxContainer).rect_position.y + ($Grid as VBoxContainer).rect_size.y < get_viewport_rect().size.y - grid_pos:
				_t = ($Tween as Tween).interpolate_property($Grid as VBoxContainer, "rect_position:y", null, get_viewport_rect().size.y - ($Grid as VBoxContainer).rect_size.y - grid_pos, tween_speed)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			yield(get_tree().create_timer(tween_speed + 0.1), "timeout")
			input_relative = 0.0


# METHODS - - - - - - - - -


func set_view() -> void:
	if view == HIDE:
		($Grid as VBoxContainer).hide()
		($Text as Label).hide()
		($Grid as VBoxContainer).rect_position.y = grid_pos
	if view == MENU:
		($Grid as VBoxContainer).show()
		($Text as Label).hide()
	elif view == CONTENT:
		($Grid as VBoxContainer).hide()
		($Text as Label).show()


func set_content(content: int) -> void:
	($Header/Title as Label).text = TITLES[content]
	($Text as Label).text = TEXTS[content]


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	if view == MENU:
		emit_signal("btn_info_back_pressed")
		yield(get_tree().create_timer(0.2), "timeout")
		view = HIDE
	elif view == CONTENT:
		view = MENU
		set_content(0)
	set_view()


func _on_Button1_pressed() -> void:
	if input_relative == 0.0:
		view = CONTENT
		set_content(1)
		set_view()


func _on_Button2_pressed() -> void:
	if input_relative == 0.0:
		view = CONTENT
		set_content(2)
		set_view()


func _on_Button3_pressed() -> void:
	if input_relative == 0.0:
		view = CONTENT
		set_content(3)
		set_view()


func _on_Button4_pressed() -> void:
	if input_relative == 0.0:
		view = CONTENT
		set_content(4)
		set_view()


func _on_Button5_pressed() -> void:
	if input_relative == 0.0:
		view = CONTENT
		set_content(5)
		set_view()
