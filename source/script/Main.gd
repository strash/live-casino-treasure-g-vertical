extends Node


var tween_speed: float = 0.3


# BUILTINS - - - - - - - - -


func _ready() -> void:
	randomize()

	var _btn_slot_pressed = ($MainMenu as Control).connect("btn_slot_pressed", self, "_on_MainMenu_btn_slot_pressed")
	var _btn_info_pressed = ($MainMenu as Control).connect("btn_info_pressed", self, "_on_MainMenu_btn_info_pressed")

	var _btn_info_back_pressed = ($Info as Control).connect("btn_info_back_pressed", self, "_on_Info_btn_info_back_pressed")

	var _level1_pressed = ($Levels as Control).connect("level1_pressed", self, "_on_MyLevels_level1_pressed")
	var _level2_pressed = ($Levels as Control).connect("level2_pressed", self, "_on_MyLevels_level2_pressed")
	var _level3_pressed = ($Levels as Control).connect("level3_pressed", self, "_on_MyLevels_level3_pressed")

	var _btn_menu_pressed = ($GUI as Control).connect("btn_menu_pressed", self, "_on_MyGUI_btn_menu_pressed")
	var _btn_back_pressed = ($GUI as Control).connect("btn_back_pressed", self, "_on_MyGUI_btn_back_pressed")
	var _btn_spin_pressed = ($GUI as Control).connect("btn_spin_pressed", self, "_on_MyGUI_btn_spin_pressed")
	var _btn_autospin_pressed = ($GUI as Control).connect("btn_autospin_pressed", self, "_on_MyGUI_btn_autospin_pressed")

	var _score_increased = ($Game as Control).connect("score_increased", self, "_on_MyGame_score_increased")
	var _spinnig_changed = ($Game as Control).connect("spinnig_changed", self, "_on_MyGame_spinnig_changed")
	var _auto_spinnig_changed = ($Game as Control).connect("auto_spinnig_changed", self, "_on_MyGame_auto_spinnig_changed")
	var _autospin_count_changed = ($Game as Control).connect("autospin_count_changed", self, "_on_MyGame_autospin_count_changed")

	set_view(1)
	($GUI as Control).call("show_hide_controls", false)


# METHODS - - - - - - - - -


func set_view(view: int) -> void:
	var _t: int
	match view:
		1:
			($Levels as Control).show()
			_t = ($Tween as Tween).interpolate_property($Levels, "modulate:a", 0.0, 1.0, tween_speed)
			_t = ($Tween as Tween).interpolate_property($Game, "modulate:a", 1.0, 0.0, tween_speed)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			yield(get_tree().create_timer(tween_speed), "timeout")
			($Game as Control).hide()
		2:
			($Game as Control).show()
			_t = ($Tween as Tween).interpolate_property($Levels, "modulate:a", 1.0, 0.0, tween_speed)
			_t = ($Tween as Tween).interpolate_property($Game, "modulate:a", 0.0, 1.0, tween_speed)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			yield(get_tree().create_timer(tween_speed), "timeout")
			($Levels as Control).hide()


# SIGNALS - - - - - - - - -


func _on_MainMenu_btn_slot_pressed() -> void:
	($Info as Control).hide()


func _on_MainMenu_btn_info_pressed() -> void:
	($Info as Control).set("view", ($Info as Control).MENU)
	($Info as Control).show()
	($Info as Control).call("set_view")


func _on_Info_btn_info_back_pressed() -> void:
	($MainMenu as Control).call("show_view", true)


func _on_MyLevels_level1_pressed() -> void:
	set_view(2)
	($Game as Control).call("prepare_level", 1)
	($GUI as Control).call("show_hide_controls", true)


func _on_MyLevels_level2_pressed() -> void:
	set_view(2)
	($Game as Control).call("prepare_level", 2)
	($GUI as Control).call("show_hide_controls", true)


func _on_MyLevels_level3_pressed() -> void:
	set_view(2)
	($Game as Control).call("prepare_level", 3)
	($GUI as Control).call("show_hide_controls", true)


func _on_MyGUI_btn_menu_pressed() -> void:
	($MainMenu as Control).call("show_view", true)


func _on_MyGUI_btn_back_pressed() -> void:
	set_view(1)
	($Game as Control).call("clear_level")
	($GUI as Control).call("show_hide_controls", false)


func _on_MyGUI_btn_spin_pressed() -> void:
	($Game as Control).call("spin")


func _on_MyGUI_btn_autospin_pressed() -> void:
	($Game as Control).call("auto_spin")


func _on_MyGame_score_increased() -> void:
	($GUI as Control).call("set_score")


func _on_MyGame_spinnig_changed(status: bool) -> void:
	($GUI as Control).set("spinning", status)


func _on_MyGame_auto_spinnig_changed(status: bool) -> void:
	($GUI as Control).set("auto_spinning", status)
	if not status:
		($GUI as Control).call("show_hide_autospin_progress", false)


func _on_MyGame_autospin_count_changed(auto_spin_count: int, auto_spin_max: int) -> void:
	($GUI as Control).call("change_autospin_count", auto_spin_count, auto_spin_max)
