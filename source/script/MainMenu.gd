extends Control


signal btn_slot_pressed
signal btn_info_pressed


var tween_speed: float = 0.2


# BUILTINS - - - - - - - - -


#func _ready() -> void:
#	pass


# METHODS - - - - - - - - -


func show_view(state: bool) -> void:
	var _t: int
	if state:
		($"." as Control).show()
		_t = ($Tween as Tween).interpolate_property($"." as Control, "modulate:a", null, 1.0, tween_speed)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property($"." as Control, "modulate:a", null, 0.0, tween_speed)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(tween_speed), "timeout")
		($"." as Control).hide()


# SIGNALS - - - - - - - - -


func _on_BtnSlot_pressed() -> void:
	show_view(false)
	emit_signal("btn_slot_pressed")


func _on_BtnInfo_pressed() -> void:
	show_view(false)
	emit_signal("btn_info_pressed")
