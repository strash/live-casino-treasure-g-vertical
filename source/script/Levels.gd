extends Control


signal level1_pressed
signal level2_pressed
signal level3_pressed


var page_pos: float
var tween_speed: float = 0.15
var input_relative: float = 0.0


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var view_w: float = get_viewport_rect().size.x
	($Grid as VBoxContainer).rect_position.x = (view_w - ($Grid as VBoxContainer).rect_size.x) / 2.0
	($Grid2 as VBoxContainer).rect_position.x = (view_w - ($Grid2 as VBoxContainer).rect_size.x) / 2.0 + view_w
	page_pos = ($Grid as VBoxContainer).rect_position.x


func _input(event: InputEvent) -> void:
	if event is InputEventScreenDrag:
		($Grid as VBoxContainer).rect_position.x += event.relative.x
		($Grid2 as VBoxContainer).rect_position.x += event.relative.x
		input_relative = event.relative.x
	elif event is InputEventScreenTouch and not event.is_pressed():
		set_page()


# METHODS - - - - - - - - -


func set_page() -> void:
	var view_w: float = get_viewport_rect().size.x
	var _t: int
	if ($Grid as VBoxContainer).rect_position.x + ($Grid as VBoxContainer).rect_size.x / 2.0 <= 0.0:
		_t = ($Tween as Tween).interpolate_property($Grid as VBoxContainer, "rect_position:x", null, page_pos - view_w, tween_speed)
		_t = ($Tween as Tween).interpolate_property($Grid2 as VBoxContainer, "rect_position:x", null, page_pos, tween_speed)
		($Paging as TextureRect).texture = load("res://source/assets/image/btn/paging_2.png")
	else:
		_t = ($Tween as Tween).interpolate_property($Grid as VBoxContainer, "rect_position:x", null, page_pos, tween_speed)
		_t = ($Tween as Tween).interpolate_property($Grid2 as VBoxContainer, "rect_position:x", null, page_pos + view_w, tween_speed)
		($Paging as TextureRect).texture = load("res://source/assets/image/btn/paging_1.png")
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(tween_speed + 0.1), "timeout")
	input_relative = 0.0


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level1_pressed")


func _on_BtnLvl2_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level2_pressed")


func _on_BtnLvl3_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level3_pressed")
